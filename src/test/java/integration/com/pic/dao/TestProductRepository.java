package integration.com.pic.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.pic.dao.ProductRepository;
import com.pic.model.Product;

@RunWith(SpringJUnit4ClassRunner.class) // 讓測試運行於Spring測試環境
//@DataJpaTest // 使用的是Memory DB進行測試
@ContextConfiguration(classes = ConfigDao.class)//引入配置文件
public class TestProductRepository {

	@Autowired
	private ProductRepository productRepository;

    @Before
    public void init(){
//    	Product product = new Product();
//    	productRepository.save(null);
    }

	@Test
	public void test() {
		Product one = productRepository.getOne("st001");
		System.out.println(one.toString());
	}
	
}
