package integration.com.pic.dao;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan(basePackages = { "com.pic.model" }) // 用來掃描Entity定義
@EnableJpaRepositories(basePackages = "com.pic.dao") // 用來掃描Repository定義
public class ConfigDao {

}
