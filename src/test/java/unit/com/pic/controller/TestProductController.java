package unit.com.pic.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.is;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import com.pic.Application;
import com.pic.controller.ProductController;
import com.pic.model.Product;
import com.pic.service.IProductService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class TestProductController {

	// 模擬http請求方便測試
	private MockMvc mockMvc;

	@Autowired
	private ProductController productController;

	@MockBean
	private IProductService IProductService;

	@Before
	public void setup() {
		// 通過參數指定controller
		this.mockMvc = standaloneSetup(this.productController).build();
	}

	@Test
	public void test() throws Exception {

//		when(this.IProductService.findProductByCategory("tableware")).thenReturn(new ArrayList<Product>());
		when(IProductService.findProductById("st001"))
				.thenReturn(new Product("st001", "筆記本", "簡約風文青筆記本  ●文青必備單品  ●裝備了讓你一秒變文青 ● 現在買\"筆記本\"搭配\"自動鉛筆\"立即享優惠!!!",
						"stationery", 888, 200, 200, "image_url"));

		ResultActions andExpect = mockMvc.perform(get("/p/st001").contentType(MediaType.APPLICATION_JSON))// 請求一個url連結
				.andDo(print())// 使用print印出訊息 andDo維結果處理器
				.andExpect(status().isOk())// 檢查回傳http是否200 , andExpect
				.andExpect(jsonPath("result.name", is("筆記本")));// 檢查回傳body 參數一為欄位名稱

	}

}
