package unit.com.pic.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.pic.service.IProductService;

import com.pic.Application;
import com.pic.dao.ProductRepository;
import com.pic.model.Product;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class TestProductService {

	@Autowired
	private IProductService iProductService;

//	@Autowired
	@MockBean // 利用mock模擬摸擬物件
	private ProductRepository productRepository;

	@Test
//	@Transactional // 託管事務 , 可解決Test時取用資料庫no session 問題
	public void test() {
		Product product = new Product();

		// 指定呼叫該方法指定回傳特定物件
		when(productRepository.getOne("st001"))
				.thenReturn(new Product("st001", "筆記本", "簡約風文青筆記本  ●文青必備單品  ●裝備了讓你一秒變文青 ● 現在買\"筆記本\"搭配\"自動鉛筆\"立即享優惠!!!",
						"stationery", 888, 200, 200, "image_url"));

		int price = iProductService.findProductById("st001").getPrice();

		// 比對是否一樣
		assertEquals(888, price);

	}

}
