package com.pic.errorHandler;

import java.io.IOException;
import java.util.Scanner;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class MyErrorHandler extends DefaultResponseErrorHandler {
	 
    @Override
     public boolean hasError(ClientHttpResponse response) throws IOException{
        return super.hasError(response);
    }
 
    @Override
    public void handleError(ClientHttpResponse response) throws IOException{
    	if(response.getStatusCode().value() == HttpStatus.BAD_REQUEST.value()) {
    		//如果回傳400 Bad Request就不繼續動作
    	}else{
        	//其餘則丟出exception
            super.handleError(response);
        }
    }
}
