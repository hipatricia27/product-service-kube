package com.pic.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pic.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {

}
