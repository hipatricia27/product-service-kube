package com.pic.service;

import java.util.List;

import com.pic.model.Product;

public interface IProductService {
	
	public List<Product> findAllProduct() ;
	
	public List<Product> findProductByCategory(final String category) ;
	
	public Product findProductById(final String productId) ;
	
	public void updateProduct(final String id, final int remain) ;

}
