package com.pic.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pic.dao.ProductRepository;
//import com.pic.datasourceMaster.ProductRepository;
//import com.pic.datasourceSlave.ProductSlaveRepository;
import com.pic.model.Product;

@Service("productService")
public class ProductServiceImpl implements IProductService {

	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public List<Product> findAllProduct()  {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Product> findProductByCategory(String category)  {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Product findProductById(String productId)  {
		// TODO Auto-generated method stub
		return productRepository.getOne(productId);
	}

	@Override
	public void updateProduct(String id, int remain)  {
		// TODO Auto-generated method stub
		
	}
	
//	@Autowired
//	ProductRepository ProductRepository;
//	
//	@Autowired
//	ProductSlaveRepository productSlaveRepository;

//	@Override
//	public List<Product> findAllProduct() throws Exception {
//		List<Product> productList = null;
//		try {
//			productList = productSlaveRepository.findAll();
//		} catch (Exception e) {
//			throw e;
//		} /* end of try-catch */
//		return productList;
//	}//end of findAllProduct
//
//	@Override
//	public List<Product> findProductByCategory(String category) throws Exception {
//		List<Product> productList = null;
//		try {
//			productList = productSlaveRepository.findProductByCategory(category);
//		} catch (Exception e) {
//			throw e;
//		} /* end of try-catch */
//		return productList;
//	}//end of findProductByCategory
//
//	@Override
//	public Product findProductById(String productId) throws Exception {
//		Product product = null;
//		try {
//			product = productSlaveRepository.findProductById(productId);
//		} catch (Exception e) {
//			throw e;
//		} /* end of try-catch */
//		return product;
//	}//end of findProductById
//
//	@Override
//	public void updateProduct(String id, int remain) throws Exception {
//		try {
//			int num = ProductRepository.updateProductById(id, remain);
//			System.out.println("num==="+num);
//		}catch(Exception e) {
//			throw e;
//		}
//	}
}//end of class
