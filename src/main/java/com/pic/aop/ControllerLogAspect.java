package com.pic.aop;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.pic.controller.CheckRole;


@Aspect
@Component
public class ControllerLogAspect {

	private final static Logger log = LoggerFactory.getLogger(ControllerLogAspect.class);
	private static final String AUTHORIZE_TOKEN = "Authorization";
	private static final String USER_ID = "user_id";
	
	@Value("${security-server.url}")
	private String url;
	
	@Autowired
	private RestTemplate restTemplate;

	@Pointcut("execution( * com.pic.controller.Test*.*(..))")
	public void controllerAuth() {
	}

	@Pointcut("execution( * com.pic.controller..*(..))")
	public void controllerLog() {
	}

	/**
	 * 處理前
	 */
	@Before("controllerLog()")
	public void runBefore(JoinPoint joinPoint) {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		String requestURL = request.getRequestURL().toString();
		if(request.getQueryString() != null) {
			requestURL=requestURL+ "?" + request.getQueryString();
		}
		Signature signature = joinPoint.getSignature();
		log.info("==" + signature.getDeclaringTypeName() + " - " + signature.getName() + " Start== "+requestURL);
	}
	
	@Around("controllerAuth()")
	public Object around(ProceedingJoinPoint pjp) throws Throwable {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();

		
		try {
			String authorization = request.getHeader(AUTHORIZE_TOKEN);
				
			// --驗證token--------------------//
			String[] split_token = authorization.split("\\s+");
			String token = split_token[1];
			System.out.println("token===>"+token);
			String securityUrl = url;
			
			// header
			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.add("Authorization", "Basic YmFlbC1jbGllbnQ6YmFlbC1zZWNyZXQ="); // bael-client bael-secret
			
			//body
			MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
			requestBody.add("token", token);
			
			// HttpEntity
			HttpEntity<MultiValueMap> requestEntity = new HttpEntity<MultiValueMap>(requestBody, requestHeaders);
			
			//HttpMethod.POST
			ResponseEntity<String> result = restTemplate.postForEntity(securityUrl, requestEntity, String.class);
			
			//token check
			if (result.getStatusCodeValue() == HttpStatus.BAD_REQUEST.value()) {
				HttpServletResponse responseN = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
				responseN.sendError(HttpStatus.UNAUTHORIZED.value(), "token check error");
				return null;
			}
			
			//check user_id
			String userId = request.getHeader(USER_ID);
			JSONObject j = new JSONObject(result.getBody());
			String tokenUserId = j.getString("user_id");
			if(!tokenUserId.equals(userId)) {
				response.sendError(HttpStatus.UNAUTHORIZED.value(), "user id check error");
				return null;
			}

			//get user permission
			JSONArray permission = (JSONArray) CheckRole.check_role(tokenUserId);
			if(permission==null) {
				response.sendError(HttpStatus.UNAUTHORIZED.value(), "role permission check error");
				return null;
			}
			
			//check url & user permission
			String requestURL = request.getRequestURL().toString();
//			String[] strs=requestURL.split("/");
//			String method = strs[strs.length-1].toString();
			
			for(int i=0;i < permission.length();i++) {
				if(requestURL.contains((CharSequence) permission.get(i))) {
					return pjp.proceed(); //continue run
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		//check role
		response.sendError(HttpStatus.UNAUTHORIZED.value(), "token check error");
		return null; 
		
	}

    
    /**
	 * 處理完成，返回内容
	 * 
	 * @param ret
	 */
	@AfterReturning(returning = "ret", pointcut = "controllerLog()")
	public void doAfterReturning(Object ret) {
//		System.out.println("@AfterReturning doAfterReturning 方法的返回值 : " + ret);
	}

	/**
	 * 異常通知
	 */
	@AfterThrowing("controllerLog()")
	public void throwss(JoinPoint jp) {
//		System.out.println("@AfterThrowing controllerLog 方法異常時執行.....");
	}

	/**
	 * 最後一定會執行 finally
	 */
	@After("controllerLog()")
	public void after(JoinPoint jp) { 
        Signature signature = jp.getSignature();  
        log.info("=="+signature.getDeclaringTypeName()+" - "+signature.getName()+" End==");
//		System.out.println("@After controllerLog 方法最後執行.....");
	}

}