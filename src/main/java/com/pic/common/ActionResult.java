package com.pic.common;

import java.io.Serializable;

public class ActionResult implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private boolean status; //狀態
	
	private Object result; //回傳物件
	
	private String message; //訊息

	/**
	 * 
	 * @param status 狀態
	 * @param result 物件
	 * @param message 訊息
	 */
	public ActionResult(boolean status, Object result, String message) {
		this.status = status;
		this.result = result;
		this.message = message;
	}
	
	/**
	 * 
	 * @param status 狀態
	 * @param message 訊息
	 */
	public ActionResult(boolean status, String message) {
		this.status = status;
		this.message = message;
	}
	
	public ActionResult() {

	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
}
