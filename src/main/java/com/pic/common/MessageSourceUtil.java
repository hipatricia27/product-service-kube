package com.pic.common;

public class MessageSourceUtil {
	final public static boolean booleanSuccess = true;
	final public static boolean booleanFail = false;

	// success
	/** B310001 查詢資料成功。 */
	final public static String B310001 = "B310001 查詢資料成功。";
	/** B320001 新增資料成功。 */
	final public static String B320001 = "B320001 新增資料成功。";
	/** B340001 更新資料成功。 */
	final public static String B340001 = "B340001 更新資料成功。";
	/** B390001 刪除資料成功。 */
	final public static String B390001 = "B390001 刪除資料成功。";

	// error
	/** B311002 查詢資料失敗-權限不足。 */
	final public static String B311002 = "B311002 查詢資料失敗-權限不足。";
	/** B321999 新增資料失敗。 */
	final public static String B321999 = "B321999 新增資料失敗。";
	/** B341003 修改資料失敗-資料內容錯誤。 */
	final public static String B341003 = "B341003 修改資料失敗-資料內容錯誤。";
	/** B391999 刪除資料失敗。 */
	final public static String B391999 = "B391999 刪除資料失敗。";
	/** S101001 AP主機連結失敗-請通知系統人員。 */
	final public static String S101001 = "S101001 AP主機連結失敗-請通知系統人員。";

}
