package com.pic.common;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.data.redis.core.query.SortQuery;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;


//@Component
public class RedisUtil {

//	@Resource
//	private RedisTemplate<String, Object> redisTemplate;
//
//	/**
//	 * 獲取key
//	 *
//	 * @param key
//	 * @return
//	 */
//	public Set<String> keys(String key) {
//			return redisTemplate.keys(key);
//	}
//
//	/**
//	 * 指定緩存失效時間
//	 *
//	 * @param key
//	 * @param time
//	 *            時間(秒)
//	 * @return
//	 */
//	public boolean expire(String key, long time) {
//		try {
//			if (time > 0) {
//				redisTemplate.expire(key, time, TimeUnit.SECONDS);
//
//			}
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//	}
//
//	/**
//	 * 根据key 獲取過期時間
//	 *
//	 * @param key
//	 *            (not null)
//	 * @return 時間(秒) 返回0代表永久有效
//	 */
//	public long getExpire(String key) {
//		return redisTemplate.getExpire(key, TimeUnit.SECONDS);
//	}
//
//	/**
//	 * 判断key是否存在
//	 *
//	 * @param key
//	 * @return true 存在 false不存在
//	 */
//	public boolean hasKey(String key) {
//		try {
//			return redisTemplate.hasKey(key);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//	}
//
//	/**
//	 * 删除缓存
//	 *
//	 * @param key
//	 *            可以傳一個或多個值
//	 */
//	@SuppressWarnings("unchecked")
//	public void del(String... key) {
//		if (key != null && key.length > 0) {
//			if (key.length == 1) {
//				redisTemplate.delete(key[0]);
//			} else {
//				redisTemplate.delete(CollectionUtils.arrayToList(key));
//			}
//		}
//	}
//
//	/**
//	 * 普通缓存獲取
//	 *
//	 * @param key
//	 * @return value
//	 */
//	public Object get(String key) {
//		return key == null ? null : redisTemplate.opsForValue().get(key);
//	}
//
//	/**
//	 * 普通缓存放入
//	 *
//	 * @param key
//	 * @param value
//	 * @return true成功 false失敗
//	 */
//	public boolean set(String key, Object value) {
//		try {
//			redisTemplate.opsForValue().set(key, value);
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//
//	}
//
//	/**
//	 * 普通缓存放入並設置時間
//	 *
//	 * @param key
//	 * @param value
//	 * @param time
//	 *            時間(秒) time要大於0 如果 time<= 0 為無限期
//	 * @return true成功 false 失敗
//	 */
//	public boolean set(String key, Object value, long time) {
//		try {
//			if (time > 0) {
//				redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
//			} else {
//				set(key, value);
//			}
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//	}
//
//	/**
//	 * 遞增
//	 *
//	 * @param key
//	 * @param delta
//	 *            要增加幾(大於0)
//	 * @return
//	 */
//	public long incr(String key, long delta) {
//		if (delta < 0) {
//			throw new RuntimeException("遞增因子必須大於0");
//		}
//		return redisTemplate.opsForValue().increment(key, delta);
//	}
//
//	/**
//	 * 遞减
//	 *
//	 * @param key
//	 * @param delta
//	 *            要減少幾(小於0)
//	 * @return
//	 */
//	public long decr(String key, long delta) {
//		if (delta < 0) {
//			throw new RuntimeException("遞減因子必須大於0");
//		}
//		return redisTemplate.opsForValue().increment(key, -delta);
//	}
//
//	// ================================Map=================================
//
//	/**
//	 * HashGet
//	 *
//	 * @param key
//	 *            (not null)
//	 * @param item
//	 *            (not null)
//	 * @return value
//	 */
//	public Object hget(String key, String item) {
//		return redisTemplate.opsForHash().get(key, item);
//	}
//
//	/**
//	 * 獲取hashKey對應的所有key值
//	 *
//	 * @param key
//	 * @return 對應的多個key值
//	 */
//	public Map<Object, Object> hmget(String key) {
//		return redisTemplate.opsForHash().entries(key);
//	}
//
//	/**
//	 * HashSet
//	 *
//	 * @param key
//	 * @param map
//	 *            對應的多個key值
//	 * @return true 成功 false 失敗
//	 */
//	public boolean hmset(String key, Map<String, Object> map) {
//		try {
//			redisTemplate.opsForHash().putAll(key, map);
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//	}
//
//	/**
//	 * HashSet 並設置時間
//	 *
//	 * @param key
//	 * @param map
//	 *            对应多个键值
//	 * @param time
//	 *            時間(秒)
//	 * @return true成功 false失败
//	 */
//	public boolean hmset(String key, Map<String, Object> map, long time) {
//		try {
//			redisTemplate.opsForHash().putAll(key, map);
//			if (time > 0) {
//				expire(key, time);
//			}
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//	}
//
//	/**
//	 * 向一張hash表中放入數據,如果不存在將創建
//	 *
//	 * @param key
//	 * @param item
//	 * @param value
//	 * @return true 成功 false失敗
//	 */
//	public boolean hset(String key, String item, Object value) {
//		try {
//			redisTemplate.opsForHash().put(key, item, value);
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//	}
//
//	/**
//	 * 向一張hash表中放入數據,如果不存在將創建
//	 *
//	 * @param key
//	 * @param item
//	 * @param value
//	 * @param time
//	 *            時間(秒) 注意:如果已存在的hash表有時間,这里将会替换原有的時間
//	 * @return true 成功 false失敗
//	 */
//	public boolean hset(String key, String item, Object value, long time) {
//		try {
//			redisTemplate.opsForHash().put(key, item, value);
//			if (time > 0) {
//				expire(key, time);
//			}
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//	}
//
//	/**
//	 * 删除hash表中的值
//	 *
//	 * @param key (not null)
//	 * @param item (可多個但不能為null)
//	 */
//	public void hdel(String key, Object... item) {
//		redisTemplate.opsForHash().delete(key, item);
//	}
//
//	/**
//	 * 判斷hash表中是否有該項的值
//	 *
//	 * @param key (not null)
//	 * @param item (not null)
//	 * @return true存在 false不存在
//	 */
//	public boolean hHasKey(String key, String item) {
//		return redisTemplate.opsForHash().hasKey(key, item);
//	}
//
//	/**
//	 * hash 累加 (如果不存在,就会創建一個 並把新增後的值返回)
//	 *
//	 * @param key
//	 * @param item
//	 * @param by 增加多少(>0)
//	 *            
//	 * @return
//	 */
//	public double hincr(String key, String item, double by) {
//		return redisTemplate.opsForHash().increment(key, item, by);
//	}
//
//	/**
//	 * hash 累減
//	 *
//	 * @param key
//	 * @param item
//	 * @param by 減少多少(>0)
//	 *            
//	 * @return
//	 */
//	public double hdecr(String key, String item, double by) {
//		return redisTemplate.opsForHash().increment(key, item, -by);
//	}
//
//	// ============================set=============================
//
//	/**
//	 * 根据key獲取Set中的所有值
//	 *
//	 * @param key
//	 * @return
//	 */
//	public Set<Object> sGet(String key) {
//		try {
//			return redisTemplate.opsForSet().members(key);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	/**
//	 * 根據value從一個set中查詢,是否存在
//	 *
//	 * @param key
//	 * @param value
//	 * @return true存在 false不存在
//	 */
//	public boolean sHasKey(String key, Object value) {
//		try {
//			return redisTemplate.opsForSet().isMember(key, value);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//	}
//
//	/**
//	 * 將數據放入set缓存
//	 *
//	 * @param key
//	 * @param values (可以多個)
//	 * @return 成功個數
//	 */
//	public long sSet(String key, Object... values) {
//		try {
//			return redisTemplate.opsForSet().add(key, values);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return 0;
//		}
//	}
//	
//	/**
//	 * 將數據放入set缓存(boundXXXOps)
//	 *
//	 * @param key
//	 * @param values
//	 * @return 成功個數
//	 */
//	public long bSet(String key, Object values) {
//		try {
//			return redisTemplate.boundSetOps(key).add(values);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return 0;
//		}
//	}
//
//	/**
//	 * 將數據放入set缓存
//	 *
//	 * @param key
//	 * @param time (秒)
//	 * @param values (可以多個)
//	 * @return 成功個數
//	 */
//	public long sSetAndTime(String key, long time, Object... values) {
//		try {
//			Long count = redisTemplate.opsForSet().add(key, values);
//			if (time > 0)
//				expire(key, time);
//			return count;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return 0;
//		}
//	}
//	
//	
//
//	/**
//	 * 獲取set緩存的長度
//	 *
//	 * @param key
//	 * @return
//	 */
//	public long sGetSetSize(String key) {
//		try {
//			return redisTemplate.opsForSet().size(key);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return 0;
//		}
//	}
//
//	/**
//	 * 移除值為value
//	 *
//	 * @param key
//	 * @param values (可以多個)
//	 * @return 移除個數
//	 */
//	public long setRemove(String key, Object... values) {
//		try {
//			Long count = redisTemplate.opsForSet().remove(key, values);
//			return count;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return 0;
//		}
//	}
//	// ===============================list=================================
//
//	/**
//	 * 獲取list缓存的内容
//	 *
//	 * @param key
//	 * @param start
//	 * @param end 结束 0 到 -1代表所有值
//	 * @return
//	 */
//	public List<Object> lGet(String key, long start, long end) {
//		try {
//			return redisTemplate.opsForList().range(key, start, end);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	/**
//	 * 獲取list缓存的長度
//	 *
//	 * @param key
//	 * @return
//	 */
//	public long lGetListSize(String key) {
//		try {
//			return redisTemplate.opsForList().size(key);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return 0;
//		}
//	}
//
//	/**
//	 * 通過索引 獲取list中的值
//	 *
//	 * @param key
//	 * @param index
//	 *             index>0時， 0 表頭，1 第二個元素，依次類推；index<0時，-1，表尾，-2倒数第二個元素，依次類推
//	 * @return
//	 */
//	public Object lGetIndex(String key, long index) {
//		try {
//			return redisTemplate.opsForList().index(key, index);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	/**
//	 * 將list放入緩存
//	 *
//	 * @param key
//	 * @param value
//	 * @return
//	 */
//	public boolean lSet(String key, Object value) {
//		try {
//			redisTemplate.opsForList().rightPush(key, value);
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//	}
//
//	/**
//	 * 將list放入緩存
//	 *
//	 * @param key
//	 * @param value
//	 * @param time (秒)
//	 * @return
//	 */
//	public boolean lSet(String key, Object value, long time) {
//		try {
//			redisTemplate.opsForList().rightPush(key, value);
//			if (time > 0)
//				expire(key, time);
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//	}
//
//	/**
//	 * 將list放入緩存
//	 *
//	 * @param key
//	 * @param value
//	 * @return
//	 */
//	public boolean lSet(String key, List<Object> value) {
//		try {
//			redisTemplate.opsForList().rightPushAll(key, value);
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//	}
//
//	/**
//	 * 將list放入緩存
//	 *
//	 * @param key
//	 * @param value
//	 * @param time (秒)
//	 * @return
//	 */
//	public boolean lSet(String key, List<Object> value, long time) {
//		try {
//			redisTemplate.opsForList().rightPushAll(key, value);
//			if (time > 0)
//				expire(key, time);
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//	}
//
//	/**
//	 * 根據索引修改list中的某條數據
//	 *
//	 * @param key
//	 * @param index
//	 * @param value
//	 * @return
//	 */
//	public boolean lUpdateIndex(String key, long index, Object value) {
//		try {
//			redisTemplate.opsForList().set(key, index, value);
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return false;
//		}
//	}
//
//	/**
//	 * 移除N個值為value
//	 *
//	 * @param key
//	 * @param count (指定移除數量)
//	 * @param value
//	 * @return 移除個數
//	 */
//	public long lRemove(String key, long count, Object value) {
//		try {
//			Long remove = redisTemplate.opsForList().remove(key, count, value);
//			return remove;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return 0;
//		}
//	}
//	
//	// ===============================sort=================================
//	
//	public List<Object> sort(SortQuery<String> sortQuery) {
//		try {
//			return redisTemplate.sort(sortQuery);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		}
//	}

}
