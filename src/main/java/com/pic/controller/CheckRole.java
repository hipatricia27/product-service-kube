package com.pic.controller;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;




@RestController
public class CheckRole {
	
	@GetMapping("checkRole")
	public static Object check_role(String userid) {
		JSONObject j;
	    try {
	      String tmp = "{\"role_permission\":[{\"userid\": \"u001\",\"permission\":[\"add\",\"get\",\"update\",\"delete\"]},{\"userid\":\"u002\",\"permission\":[\"get\"]}]}";  

	      j = new JSONObject(tmp);
	      JSONArray role_p = j.getJSONArray("role_permission");
	      
	      for(int i=0;i < role_p.length();i++) {
	    	  JSONObject role = (JSONObject) role_p.get(i);
		      String jsonUserid = role.get("userid").toString();
		      System.out.println("jsonUserid==>"+jsonUserid);
		      
		      if(jsonUserid.equals(userid)) {
		    	  JSONArray permission = (JSONArray) role.get("permission");
		    	  System.out.println("permission==>"+permission.get(0));
		    	  return permission;
		      }
	      }
	    }catch(Exception e){
	    System.err.println("Error: " + e.getMessage());
	  }
		return null;
	}

}
