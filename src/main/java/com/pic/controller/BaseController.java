package com.pic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pic.common.MessageSourceUtil;

@RestController
@RequestMapping
public class BaseController {
	
	@Autowired
	protected MessageSourceUtil messageSourceUtil;

	
}
