package com.pic.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	
	@GetMapping("getProduct")
	public String getProduct() {
		System.out.println("-> Product Get !");

		return "-> Product Get !!!! ";
	}
	
	@GetMapping("addProduct")
	public String addProduct() {
		System.out.println("-> Product Add !");

		return "-> Product Add !!!! ";
	}

}
