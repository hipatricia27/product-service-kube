package com.pic.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pic.common.ActionResult;
import com.pic.common.MessageSourceUtil;
import com.pic.model.Product;
import com.pic.service.IProductService;

import io.opentracing.Scope;
import io.opentracing.Span;
import io.opentracing.Tracer;

@RestController
@RequestMapping
public class ProductController extends BaseController {
	private final static Logger log = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	IProductService productService;

	@Autowired
	private Tracer tracer;

	/**
	 * 取得所有商品清單
	 * 
	 * @RequestMethod GET
	 * @return
	 * @throws InterruptedException
	 */
	@RequestMapping(value = "/", method = { RequestMethod.GET })
	public ActionResult findAllProduct() throws InterruptedException {
		List<Product> productList = new ArrayList<Product>();
		ActionResult actionResult = null;
		Product product = new Product();

		Map<String, String> map = new HashMap<>();
		map.put("log1", "aa");
		map.put("log2", "bb");
		Scope scope = tracer.scopeManager().active();
		if (scope != null) {
			scope.span().log(map);
		}

		Span span = tracer.activeSpan();
		span.context().toTraceId();

		try {
			product.setId("tw001");
			product.setName("筷子");
			product.setDescription("[逮丸 SPONCHop 木筷] ● 逮丸必買生活食器品牌, ● 天然無毒、質地溫潤, ● 防水耐用 ● 用過的小朋友都說讚  ● 回購率超高單品");
			product.setCategory("餐具");
			product.setPrice(50);
			product.setRemain(100);
			product.setTotal(100);
			product.setImage_url(
					"https://www.homeshops.com.tw/wp-content/uploads/2018/05/chabatree_thai%E7%AD%B7%E5%AD%90-1.jpg");
			productList.add(product);
			product = new Product();
			product.setId("tw002");
			product.setName("湯匙");
			product.setDescription("[逮丸 SPONCHop 湯匙] ●逮丸必買生活食器品牌 ● 天然無毒的木製食器  ● 讓食物變得更好吃 ● 用過的小朋友都說讚");
			product.setCategory("餐具");
			product.setPrice(50);
			product.setRemain(100);
			product.setTotal(100);
			product.setImage_url(
					"https://www.homeshops.com.tw/wp-content/uploads/2018/05/chabatree_okazu%E6%B9%AF%E5%8C%99-1.jpg");
			productList.add(product);
			product = new Product();
			product.setId("tw003");
			product.setName("餐盤(中)");
			product.setDescription("全新日本直送陶瓷餐盤 ●400多年歷史的日本傳統陶瓷工藝  ●邊緣平滑順手耐用\r\n" + "●底部無釉 防滑耐磨  ●高溫釉燒 色澤飽和");
			product.setCategory("餐具");
			product.setPrice(100);
			product.setRemain(100);
			product.setTotal(100);
			product.setImage_url(
					"https://img4.momomall.com.tw/goodsimg/103/030/0000/184/1030300000184_L1.jpg?t=1495460439");
			productList.add(product);
			product = new Product();
			product.setId("tw004");
			product.setName("餐盤(大)");
			product.setDescription("英國創意與創新的陶瓷餐皿品牌  ●擁有半世紀的燒瓷技術 ●美學設計與實用兼具  ●不是英國人也能體驗的貴族奢華");
			product.setCategory("餐具");
			product.setPrice(150);
			product.setRemain(100);
			product.setTotal(100);
			product.setImage_url("https://tzu.com.tw/images/201804/goods_img/55_G_1523990404836.jpg");
			productList.add(product);
			product = new Product();
			product.setId("st001");
			product.setName("筆記本");
			product.setDescription("簡約風文青筆記本  ●文青必備單品  ●裝備了讓你一秒變文青 ● 現在買\"筆記本\"搭配\"自動鉛筆\"立即享優惠!!!");
			product.setCategory("文具");
			product.setPrice(30);
			product.setRemain(200);
			product.setTotal(200);
			product.setImage_url("https://img.muji.net/img/item/4547315264964_1260.jpg");
			productList.add(product);
			product = new Product();
			product.setId("st002");
			product.setName("自動鉛筆");
			product.setDescription("歐趴不求人旋轉自動鉛筆 ●筆軸採超炫亮彩設計 ●亮麗搶眼 ●筆蓋式設計多一層保護 筆尖不易受損 ● 現在買\"筆記本\"搭配\"自動鉛筆\"立即享優惠!!!");
			product.setCategory("文具");
			product.setPrice(25);
			product.setRemain(200);
			product.setTotal(200);
			product.setImage_url(
					"https://img.udnfunlife.com/image/product/DS004857/APPROVED/DU00043579/20190425122713436_1000.jpg?t=20190527173500");
			productList.add(product);

			actionResult = new ActionResult(MessageSourceUtil.booleanSuccess, productList, MessageSourceUtil.B310001);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.toString());
		} // end of try-catch
		return actionResult;
	}// end of findAllProduct

	/**
	 * 依分類取得商品清單
	 * 
	 * @RequestMethod GET
	 * @return
	 */
	@RequestMapping(value = "/category/{category}", method = { RequestMethod.GET })
	public ActionResult getProductByCategory(@PathVariable String category) {
		List<Product> productList = null;
		ActionResult actionResult = null;

		try {
			productList = productService.findProductByCategory(category);
			for (int i = 0; i < productList.size(); i++) {
				if (productList.get(i).getDescription() == null) {
					productList.get(i).setDescription("");
				}
				if (productList.get(i).getImage_url() == null) {
					productList.get(i).setImage_url("");
				}
			}
			actionResult = new ActionResult(MessageSourceUtil.booleanSuccess, productList, MessageSourceUtil.B310001);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.toString());
		} // end of try-catch

		return actionResult;
	}// end of getProductByCategory

	/**
	 * 依產品編號取得商品清單
	 * 
	 * @RequestMethod GET
	 * @return
	 */
	@RequestMapping(value = "/p/{productId}", method = { RequestMethod.GET })
	public ActionResult getProductById(@PathVariable String productId) {
//		log.info("Call Product-Service/p");
		Product product = new Product();
		ActionResult actionResult = null;

//		product.setId("tw001");
//		product.setName("筷子");
//		product.setDescription("[逮丸 SPONCHop 木筷] ● 逮丸必買生活食器品牌, ● 天然無毒、質地溫潤, ● 防水耐用 ● 用過的小朋友都說讚  ● 回購率超高單品");
//		product.setCategory("餐具");
//		product.setPrice(50);
//		product.setRemain(100);
//		product.setTotal(100);
//		product.setImage_url(
//				"https://www.homeshops.com.tw/wp-content/uploads/2018/05/chabatree_thai%E7%AD%B7%E5%AD%90-1.jpg");
		
		product = productService.findProductById(productId);
		
		actionResult = new ActionResult(MessageSourceUtil.booleanSuccess, product, MessageSourceUtil.B310001);

		return actionResult;
	}// end of getProductById

	/**
	 * 更新商品庫存
	 * 
	 * @RequestMethod GET
	 * @return
	 */
	@RequestMapping(value = "/update", method = { RequestMethod.POST })
	public ActionResult updateProductById(@RequestParam("id") String id, @RequestParam("quantity") int quantity)
			throws Exception {
		Product product = null;
		ActionResult actionResult;
		try {
			product = productService.findProductById(id);
			Objects.requireNonNull(product, "===Product can not be null==="); // 沒有就丟出去

			if (product.getRemain() - quantity > 0) {
				productService.updateProduct(id, product.getRemain() - quantity); // 更新產品資料
				actionResult = new ActionResult(MessageSourceUtil.booleanSuccess, MessageSourceUtil.B340001);
			} else {
				actionResult = new ActionResult(MessageSourceUtil.booleanFail, MessageSourceUtil.B341003);
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.toString());
			actionResult = new ActionResult(MessageSourceUtil.booleanFail, MessageSourceUtil.B341003);
		} // end of try-catch
		return actionResult;
	}// end of getProductById

}// end of class
