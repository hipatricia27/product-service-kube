package com.pic;

import javax.naming.ConfigurationException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.pic.common.MessageSourceUtil;
import com.pic.errorHandler.MyErrorHandler;

import io.jaegertracing.internal.JaegerTracer;

@SpringBootApplication
@RestController
@EnableAutoConfiguration(exclude = { DataSourceTransactionManagerAutoConfiguration.class })
@ServletComponentScan("com.pic.aop")
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

//	@RequestMapping(value = "/")
//	public String home() {
//		return "Product Service";
//	}
	
	@Bean
	public MessageSourceUtil messageSourceUtil() {
		return new MessageSourceUtil();
	}
	
	@Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate=new RestTemplate();
        //Response status code 4XX or 5XX to the client.
        restTemplate.setErrorHandler(new MyErrorHandler());
        return restTemplate;
    }
	
//	@Bean
//	public io.opentracing.Tracer getTracer() throws ConfigurationException {
//	    return new new io.jaegertracing.Tracer.Builder("my-spring-boot-app").build();
//	}
	
//	@Bean
//	public io.opentracing.Tracer getTracer() throws ConfigurationException {
//	    return new JaegerTracer.Builder("product-service").build();
//	}
	
}